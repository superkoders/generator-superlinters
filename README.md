# SuperLinters (custom yeoman generator)
Get [Stylelint](https://www.npmjs.com/package/@superkoders/stylelint-config), [ESLint](https://www.npmjs.com/package/@superkoders/eslint-config) and [Prettier](https://www.npmjs.com/package/@superkoders/prettier-config) configurations, along with lint-staged pre-commit hook to living project with one command.

## Installation
1. Go to the project folder and run
```bash
npm init yo superlinters
```

Installation process will ask you about overriding files, even `package.json`. If you keep your files in version control, you don't have to be afraid of it. The package.json will be extended only, so it is also safe. Answer _yes_ everytime.


## Recommended settings
### Works best with relevant editor extensions
- [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
- [Stylelint](https://marketplace.visualstudio.com/items?itemName=stylelint.vscode-stylelint)

### And these settings:
Turn off default validation:
```JSON
"css.validate": false,
"less.validate": false,
"scss.validate": false,
```

Constrain Prettier use only for correctly configured projects
```JSON
"prettier.requireConfig": true,
```

If you feel like it, turn on Autosave formatting:
```JSON
"editor.formatOnSave": true,
"editor.codeActionsOnSave": {
    "source.fixAll.eslint": true,
    "source.fixAll.stylelint": true
},
```

Turn it off for unsupported languages, because it could try and fail with formatting it.
```JSON
"[twig]": {
    "editor.formatOnSave": false
},
"[njk]": {
    "editor.formatOnSave": false
},
```

## Enjoy clean code and make something nice!
