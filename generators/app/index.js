"use strict";
const Generator = require("yeoman-generator");
const chalk = require("chalk");

module.exports = class extends Generator {
	prompting() {
		this.log(
			`${chalk.red("Hello. Nice to see you want to write clean code.")}\n`
		);
	}

	writing() {
		const pkgJson = {
			devDependencies: {
				"@superkoders/eslint-config": "1.2.24",
				"@superkoders/prettier-config": "0.2.5",
				"@superkoders/stylelint-config": "1.2.3",
				"husky": "3.0.9",
				"lint-staged": "9.5.0"
			},
			scripts: {
				"lint:css": "stylelint --syntax scss \"src/**/*.{css,scss}\"; exit 0",
				"lint:css:fix": "prettier-stylelint --write \"src/**/*.{css,scss}\"",
				"lint:js": "eslint .; exit 0",
				"lint:js:fix": "eslint . --fix"
			},
			husky: {
				hooks: {
					"pre-commit": "lint-staged"
				}
			},
			"lint-staged": {
				"*.{scss}": ["stylelint --syntax scss \"*.{css,scss}\""],
				"*.js": ["eslint"]
			}
		};

		// Extend or create package.json file in destination path
		this.fs.extendJSON(this.destinationPath("package.json"), pkgJson);

		this.fs.copyTpl(
			[
				`${this.templatePath()}/**/.*`,
				`${this.templatePath()}/**/*`,
				`${this.templatePath()}/.vscode`
			],
			this.destinationPath()
		);
	}

	Install() {
		this.installDependencies({
			npm: true,
			bower: false,
			yarn: false
		});
	}

	end() {
		console.log(
			`\n${chalk.green(
				"Superlinters settings loaded successfuly. Yay!"
			)}\nHere are your Superlinters settings.\nMake sure your code editor is correctly setup, so you can focus on the things that matter. You can look here for instructions: ${chalk.blue(
				"https://superkoders.gitbook.io/wiki/technicke/programy-a-nastaveni/linter-prettier/"
			)}\nAlso, don't forget to check correct path in npm scripts or ignore files, so it suit your project.
			${chalk.red(
				"\nBe careful when adding to project with some linters already installed. It might cause conficts."
			)}\n`
		);
	}
};
